<!DOCTYPE html>
<html class="wf-freighttextpro-n5-active wf-freightdisplaypro-n5-active wf-freightsanspro-n4-active wf-freighttextpro-i4-active wf-firamono-n4-active wf-freightsanscondensedpro-n5-active wf-freightsanscondensedpro-n6-active wf-freightsanspro-i4-active wf-freightsanspro-n6-active wf-freightsanspro-i6-active wf-freightsanspro-n5-active wf-freighttextpro-n4-active wf-active" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Building a Simple CRUD app with Node, Express, and MongoDB | Zell Liew</title>
<meta name="description" content="
  Mega walkthrough on what CRUD, Express, MongoDB are. And how to use them to build a simple web application.">
<link rel="canonical" href="https://zellwk.com/blog/crud-express-mongodb/">
<link rel="stylesheet" href="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/styles-min-c2487ce387.css">

<script async="" src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/analytics.js"></script><script src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/tsd8lrs.js" async=""></script><script>
  (function(d) {
    var config = {
      kitId: 'tsd8lrs',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<!-- Favicons -->
<link rel="apple-touch-icon" sizes="180x180" href="https://zellwk.com/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://zellwk.com/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://zellwk.com/favicons/favicon-16x16.png">
<link rel="manifest" href="https://zellwk.com/favicons/manifest.json">
<link rel="mask-icon" href="https://zellwk.com/favicons/safari-pinned-tab.svg" color="#fd256e">
<link rel="shortcut icon" href="https://zellwk.com/favicons/favicon.ico">
<meta name="msapplication-config" content="/images/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

  
  <meta property="og:image" content="https://zellwk.com/images/zell-profile-pic.jpg">
  <meta name="twitter:image" content="https://zellwk.com/images/zell-profile-pic.jpg">
  <meta property="og:image:alt" content="Page image for Building a Simple CRUD app with Node, Express, and MongoDB | Zell Liew">
  <meta name="twitter:image:alt" content="Page image for Building a Simple CRUD app with Node, Express, and MongoDB | Zell Liew">



  <meta name="twitter:creator" content="@zellwk">

<link rel="alternate" type="application/rss+xml" title="Feed for articles" href="https://zellwk.com/feed.xml">
<script src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/CKJS4.js" id="_ck_js_tag"></script><style type="text/css">.tk-freight-sans-pro{font-family:"freight-sans-pro",sans-serif;}.tk-freight-text-pro{font-family:"freight-text-pro",serif;}.tk-freight-display-pro{font-family:"freight-display-pro",serif;}.tk-fira-mono{font-family:"fira-mono",monospace;}.tk-freight-sans-condensed-pro{font-family:"freight-sans-condensed-pro",sans-serif;}</style><style type="text/css">@font-face{font-family:tk-freight-sans-pro-n4;src:url(https://use.typekit.net/af/442215/000000000000000000010b5a/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/442215/000000000000000000010b5a/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/442215/000000000000000000010b5a/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-sans-pro-i4;src:url(https://use.typekit.net/af/3df5fe/000000000000000000010b5b/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff2"),url(https://use.typekit.net/af/3df5fe/000000000000000000010b5b/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff"),url(https://use.typekit.net/af/3df5fe/000000000000000000010b5b/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("opentype");font-weight:400;font-style:italic;font-display:auto;}@font-face{font-family:tk-freight-sans-pro-n5;src:url(https://use.typekit.net/af/8dd886/000000000000000000010b5c/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/8dd886/000000000000000000010b5c/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/8dd886/000000000000000000010b5c/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-sans-pro-n6;src:url(https://use.typekit.net/af/cef9f3/000000000000000000010b5e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff2"),url(https://use.typekit.net/af/cef9f3/000000000000000000010b5e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff"),url(https://use.typekit.net/af/cef9f3/000000000000000000010b5e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("opentype");font-weight:600;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-sans-pro-i6;src:url(https://use.typekit.net/af/2a72d2/000000000000000000010b5f/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i6&v=3) format("woff2"),url(https://use.typekit.net/af/2a72d2/000000000000000000010b5f/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i6&v=3) format("woff"),url(https://use.typekit.net/af/2a72d2/000000000000000000010b5f/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i6&v=3) format("opentype");font-weight:600;font-style:italic;font-display:auto;}@font-face{font-family:tk-freight-text-pro-n4;src:url(https://use.typekit.net/af/ac6334/000000000000000000012059/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/ac6334/000000000000000000012059/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/ac6334/000000000000000000012059/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-text-pro-i4;src:url(https://use.typekit.net/af/5464d5/00000000000000000001205a/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff2"),url(https://use.typekit.net/af/5464d5/00000000000000000001205a/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff"),url(https://use.typekit.net/af/5464d5/00000000000000000001205a/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("opentype");font-weight:400;font-style:italic;font-display:auto;}@font-face{font-family:tk-freight-text-pro-n5;src:url(https://use.typekit.net/af/6469f1/000000000000000000012108/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/6469f1/000000000000000000012108/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/6469f1/000000000000000000012108/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-display-pro-n5;src:url(https://use.typekit.net/af/52559e/0000000000000000000132d3/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/52559e/0000000000000000000132d3/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/52559e/0000000000000000000132d3/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:tk-fira-mono-n4;src:url(https://use.typekit.net/af/f654d3/000000000000000000014766/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/f654d3/000000000000000000014766/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/f654d3/000000000000000000014766/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-sans-condensed--n5;src:url(https://use.typekit.net/af/57785c/00000000000000000001776e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/57785c/00000000000000000001776e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/57785c/00000000000000000001776e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:tk-freight-sans-condensed--n6;src:url(https://use.typekit.net/af/501d74/000000000000000000017771/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff2"),url(https://use.typekit.net/af/501d74/000000000000000000017771/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff"),url(https://use.typekit.net/af/501d74/000000000000000000017771/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("opentype");font-weight:600;font-style:normal;font-display:auto;}</style><style type="text/css">@font-face{font-family:freight-sans-pro;src:url(https://use.typekit.net/af/442215/000000000000000000010b5a/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/442215/000000000000000000010b5a/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/442215/000000000000000000010b5a/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;font-display:auto;}@font-face{font-family:freight-sans-pro;src:url(https://use.typekit.net/af/3df5fe/000000000000000000010b5b/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff2"),url(https://use.typekit.net/af/3df5fe/000000000000000000010b5b/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff"),url(https://use.typekit.net/af/3df5fe/000000000000000000010b5b/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("opentype");font-weight:400;font-style:italic;font-display:auto;}@font-face{font-family:freight-sans-pro;src:url(https://use.typekit.net/af/8dd886/000000000000000000010b5c/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/8dd886/000000000000000000010b5c/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/8dd886/000000000000000000010b5c/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:freight-sans-pro;src:url(https://use.typekit.net/af/cef9f3/000000000000000000010b5e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff2"),url(https://use.typekit.net/af/cef9f3/000000000000000000010b5e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff"),url(https://use.typekit.net/af/cef9f3/000000000000000000010b5e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("opentype");font-weight:600;font-style:normal;font-display:auto;}@font-face{font-family:freight-sans-pro;src:url(https://use.typekit.net/af/2a72d2/000000000000000000010b5f/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i6&v=3) format("woff2"),url(https://use.typekit.net/af/2a72d2/000000000000000000010b5f/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i6&v=3) format("woff"),url(https://use.typekit.net/af/2a72d2/000000000000000000010b5f/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i6&v=3) format("opentype");font-weight:600;font-style:italic;font-display:auto;}@font-face{font-family:freight-text-pro;src:url(https://use.typekit.net/af/ac6334/000000000000000000012059/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/ac6334/000000000000000000012059/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/ac6334/000000000000000000012059/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;font-display:auto;}@font-face{font-family:freight-text-pro;src:url(https://use.typekit.net/af/5464d5/00000000000000000001205a/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff2"),url(https://use.typekit.net/af/5464d5/00000000000000000001205a/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("woff"),url(https://use.typekit.net/af/5464d5/00000000000000000001205a/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3) format("opentype");font-weight:400;font-style:italic;font-display:auto;}@font-face{font-family:freight-text-pro;src:url(https://use.typekit.net/af/6469f1/000000000000000000012108/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/6469f1/000000000000000000012108/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/6469f1/000000000000000000012108/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:freight-display-pro;src:url(https://use.typekit.net/af/52559e/0000000000000000000132d3/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/52559e/0000000000000000000132d3/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/52559e/0000000000000000000132d3/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:fira-mono;src:url(https://use.typekit.net/af/f654d3/000000000000000000014766/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/f654d3/000000000000000000014766/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/f654d3/000000000000000000014766/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;font-display:auto;}@font-face{font-family:freight-sans-condensed-pro;src:url(https://use.typekit.net/af/57785c/00000000000000000001776e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/57785c/00000000000000000001776e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/57785c/00000000000000000001776e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");font-weight:500;font-style:normal;font-display:auto;}@font-face{font-family:freight-sans-condensed-pro;src:url(https://use.typekit.net/af/501d74/000000000000000000017771/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff2"),url(https://use.typekit.net/af/501d74/000000000000000000017771/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("woff"),url(https://use.typekit.net/af/501d74/000000000000000000017771/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n6&v=3) format("opentype");font-weight:600;font-style:normal;font-display:auto;}</style><script type="text/javascript" src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/jquery.js"></script></head>

  <body class="">
    <div class="c-site-container">
        <header>
  <div class="l-wrap">
    <nav class="c-main-nav l-wrap__full">
      <div>
        <a class="c-main-nav__logo" href="https://zellwk.com/about">
          <div class="o-zell"><svg id="zell" viewBox="0 0 90 30">
  <title> Zell </title>
  <g>
    <g class="z">
      <path class="o-zell__bar" d="M0 0v6.332h24.336L24.384 0z"></path>
      <path class="o-zell__text" d="M24.336 6.332h-8.154L0 23.425v6.332h24.336v-6.38H9.562z"></path>
    </g>
    <path class="e o-zell__text" d="M45.706 29.757v-5.34H34.657v-3.614h9.194v-4.941h-9.194V12.28h11.05V6.956H27.893v22.801z"></path>
    <path class="l1 o-zell__text" d="M68.124 29.757v-5.9H57.938V6.956h-7.275v22.801z"></path>
    <path class="l2 o-zell__text" d="M89.646 29.757v-5.9H79.461V6.956h-7.276v22.801z"></path>
  </g>
</svg>
</div>
        </a>
      </div><div class="main-nav jsNavContainer">
    <a href="https://learnjavascript.today/" class="o-navlink">Learn JavaScript</a>
    <a href="https://zellwk.com/about" class="o-navlink">About</a>
    <a href="https://zellwk.com/blog" class="o-navlink">Articles</a>
    <a href="https://zellwk.com/contact" class="o-navlink">Contact</a>
    <a href="https://zellwk.com/newsletter" class="o-navlink">Newsletter</a>

    <button class="button c-main-nav__toggle jsOffsiteLauncher">Menu</button>
  </div>
    </nav>
  </div>
</header>


      <main><div class="l-wrap">
  <article class="o-content post-layout">
    <h1>Building a Simple CRUD app with Node, Express, and MongoDB</h1>
    <span class="post-layout__date">9th Apr 2020</span>

    
    
<p>I finally understood how to work with Node, Express, and MongoDB. I 
want to write a comprehensive tutorial so you won’t have to go through 
the same headache I went through.</p>
<!--more-->
<h2 id="crud%2C-express-and-mongodb">CRUD, Express and MongoDB</h2>
<p>CRUD, Express and MongoDB are big words for a person who has never 
touched any server-side programming in their life. Let’s quickly 
introduce what they are before we diving into the tutorial.</p>
<p><strong><a href="https://expressjs.com/">Express</a> is a framework for building web applications on top of <a href="https://nodejs.org/en/">Node.js</a></strong>.
 It simplifies the server creation process that is already available in 
Node. In case you were wondering, Node allows you to use JavaScript as 
your server-side language.</p>
<p><strong><a href="https://www.mongodb.com/">MongoDB</a> is a database</strong>. This is the place where you store information for your websites (or applications).</p>
<p><strong><a href="https://en.wikipedia.org/wiki/Create,_read,_update_and_delete">CRUD</a> is an acronym for Create, Read, Update and Delete</strong>. It is a set of operations we get servers to execute (<code>POST</code>, <code>GET</code>, <code>PUT</code> and <code>DELETE</code> requests respectively). This is what each operation does:</p>
<ul>
<li><strong>Create (POST)</strong> - Make something</li>
<li><strong>Read (GET)</strong>- Get something</li>
<li><strong>Update (PUT)</strong> - Change something</li>
<li><strong>Delete (DELETE)</strong>- Remove something</li>
</ul>
<p><code>POST</code>, <code>GET</code>, <code>PUT</code>, and <code>DELETE</code> requests let us construct <a href="https://www.smashingmagazine.com/2018/01/understanding-using-rest-api/">Rest APIs</a>.</p>
<p>If we put CRUD, Express and MongoDB together into a single diagram, this is what it would look like:</p>
<figure><img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/crud-express-mongo.png" alt="">
</figure>
<p>Does CRUD, Express and MongoDB makes more sense to you now?</p>
<p>Great. Let’s move on.</p>
<h2 id="we%E2%80%99ll-build-a-simple-application-together">We’ll build a simple application together</h2>
<p>Let’s build a simple application that lets you track a list of quotes from Star Wars Characters. Here’s what it looks like:</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/looks.png" alt="Add CSS to make the app look better.">
</figure>
<p>Free free to check out the <a href="https://crud-demo.zellwk.com/">demo</a> before continuing with this tutorial.</p>
<div class="note"><p>This article is LONG! Remember to <strong>grab the source code by leaving your name and email address in <a href="#convertkit">this form</a></strong>. I’ll also send you this article in PDF so you can read it at your leisure.</p>
</div><p>By the way, I’m not going to focus on the styles since we’re focusing on learning Crud, Express, and MongoDB in this tutorial.</p>
<h2 id="prerequisites">Prerequisites</h2>
<p>You’ll need two things to get started with this tutorial:</p>
<ol>
<li>You are not afraid of typing commands into a Command Line. If you’re afraid, use <a href="https://zellwk.com/blog/fear-of-command-line/" title="Overcoming fear of the command line">this article to get over your fear</a>.</li>
<li>You need to have <a href="https://nodejs.org/">Node</a> installed.</li>
</ol>
<p>To check if you have Node installed, open up your Command Line and run the following code:</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash">$ node -v
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/node-version.png" alt="Node version">
</figure>
<p>You should get a version number if you have Node installed. If you 
don’t, you can install Node either by downloading the installer from <a href="https://nodejs.org/">Node’s website</a> or downloading it through package managers like <a href="https://zellwk.com/blog/homebrew/">Homebrew</a> (Mac) and <a href="https://chocolatey.org/">Chocolatey</a> (Windows).</p>
<h2 id="getting-started">Getting started</h2>
<p>Start by creating a folder for this project. Feel free to call it 
anything you want. After you’ve created the folder, navigate into it 
with the Terminal and run <code>npm init</code>.</p>
<p><code>npm init</code> creates a <code>package.json</code> file which helps you manage dependencies (which we will install as we go through the tutorial).</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash">$ <span class="token function">npm</span> init
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/npm-init.png" alt="npm init.">
</figure>
<p>Just hit enter through everything that appears. I’ll talk about the ones you need to know as we go along.</p>
<h2 id="running-node-for-the-first-time-in-your-life">Running Node for the first time in your life</h2>
<p>The simplest way to use node is to run the <code>node</code> command, and specify a path to a file. Let’s create a file called <code>server.js</code> to run node with.</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">touch</span> server.js
</code></pre><div class="toolbar"></div></div>
<p>Next, put this a <code>console.log</code> statement into <code>server.js</code>. This lets us know whether Node is running properly.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token comment">// server.js</span>
console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'May Node be with you'</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Now, run <code>node server.js</code> in your command line and you should see this:</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/node-log.png" alt="Logged May Node be with you.">
</figure>
<p>Great. Node works. The next step is to learn to use Express.</p>
<h2 id="using-express">Using Express</h2>
<p>First, we have to install Express. We can do this by running the <code>npm install</code> command. (<code>npm</code> is installed with Node, which is why you use commands like <code>npm init</code> and <code>npm install</code>).</p>
<p>Run <code>npm install express --save</code> command in your command line.</p>
<div class="note"><p>The <code>--save</code> flag saves <code>express</code> as a <code>dependency</code> in <code>package.json</code>. It’s important to know these dependencies because <code>npm</code> can retrieve dependencies with another <code>npm install</code> command when you need it later.</p>
</div><div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">npm</span> <span class="token function">install</span> express --save
</code></pre><div class="toolbar"></div></div>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/exp-dependency.png" alt="Saved express as a dependency">
</figure>
<p>Next, we use express in <code>server.js</code> by requiring it.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token keyword">const</span> express <span class="token operator">=</span> <span class="token function">require</span><span class="token punctuation">(</span><span class="token string">'express'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token keyword">const</span> app <span class="token operator">=</span> <span class="token function">express</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="toolbar"></div></div>
<p>We need to create a server that browsers can connect to. We do this by using the Express’s <code>listen</code> method.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">listen</span><span class="token punctuation">(</span><span class="token number">3000</span><span class="token punctuation">,</span> <span class="token keyword">function</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'listening on 3000'</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Now, run <code>node server.js</code> and navigate to <code>localhost:3000</code> on your browser. You should see a message that says <code>cannot get /</code>.</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/cannot-get.png" alt="Cannot get /.">
</figure>
<p>That’s a good sign. It means <strong>we can now communicate to our express server through the browser</strong>. This is where we begin CRUD operations.</p>
<h2 id="crud---read">CRUD - READ</h2>
<p>Browsers perform the <strong>READ</strong> operation when you visit a website. Under the hood, they send a <strong>GET</strong> request to the server to perform this READ operation.</p>
<p>You see <code>cannot get /</code> because our server sent nothing back to the browser.</p>
<p>In Express, we handle a <strong>GET</strong> request with the <code>get</code> method:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span>endpoint<span class="token punctuation">,</span> callback<span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p><strong><code>endpoint</code> is the requested endpoint.</strong> It’s the value that comes after your domain name. Here are some examples:</p>
<ul>
<li>When you visit <code>localhost:3000</code>, you’re actually visiting <code>localhost:3000/</code>. In this case, browsers requested for <code>/</code>.</li>
<li>You’re reading this article on <code>https://zellwk.com/blog/crud-express-mongodb/</code>. The domain name is <code>zellwk.com</code>. The requested endpoint is anything that comes after <code>zellwk.com</code> (which is <code>/blog/crud-express-mongodb</code>).</li>
</ul>
<p><code>callback</code> tells the server what to do when the requested endpoint matches the endpoint stated. It takes two arguments: A <code>request</code> object and a <code>response</code> object.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token comment">// We normally abbreviate `request` to `req` and `response` to `res`.</span>
app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
  <span class="token comment">// do something here</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>For now, let’s write <code>Hello World</code> back to the browser. We do so by using a <code>send</code> method that comes with the <code>response</code> object:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token keyword">function</span><span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
  res<span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span><span class="token string">'Hello World'</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>I’m going to start writing in ES6 code and show you how to convert to ES6 along the way as well. First off, I’m replacing <code>function()</code> with an <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions">ES6 arrow function</a>. The below code is the same as the above code:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  res<span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span><span class="token string">'Hello World'</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Now, restart your server by doing the following:</p>
<ol>
<li>Stop the current server by hitting <code>CTRL + C</code> in the command line.</li>
<li>Run <code>node server.js</code> again.</li>
</ol>
<p>Then, navigate to <code>localhost:3000</code> on your browser. You should be able to see a string that says “Hello World”.</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/get-hello-world.png" alt="">
</figure>
<p>Great.</p>
<p>Next, let’s change <code>server.js</code> so we serve up an <code>index.html</code> page back to the browser. To do this, we use the <code>sendFile</code> method that’s provided by the <code>res</code> object.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  res<span class="token punctuation">.</span><span class="token function">sendFile</span><span class="token punctuation">(</span>__dirname <span class="token operator">+</span> <span class="token string">'/index.html'</span><span class="token punctuation">)</span>
  <span class="token comment">// Note: __dirname is the current directory you're in. Try logging it and see what you get!</span>
  <span class="token comment">// Mine was '/Users/zellwk/Projects/demo-repos/crud-express-mongo' for this app.</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>In the <code>sendFile</code> method above, we told Express to serve an <code>index.html</code> file that can be found in the root of your project folder. We don’t have that file yet. Let’s make it now.</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">touch</span> index.html
</code></pre><div class="toolbar"></div></div>
<p>Let’s put some text in our <code>index.html</code> file as well:</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token doctype">&lt;!DOCTYPE html&gt;</span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>html</span> <span class="token attr-name">lang</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>en<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>head</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>meta</span> <span class="token attr-name">charset</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>UTF-8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>title</span><span class="token punctuation">&gt;</span></span>MY APP<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>title</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>head</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>body</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h1</span><span class="token punctuation">&gt;</span></span> May Node and Express be with you. <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h1</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>body</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>html</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<p>Restart your server and refresh your browser. You should be able to see your HTML file now.</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/get-index.png" alt="The HTML file.">
</figure>
<p>This is how Express handles a <strong>GET</strong> request (<strong>READ</strong> operation) in a nutshell.</p>
<p>At this point, you probably have realized that you need to restart your server whenever you make a change to <code>server.js</code>. This is process is incredibly tedious, so let’s take a quick detour and streamline it by using a tool called <a href="https://nodemon.io/">nodemon</a>.</p>
<h2 id="enter-nodemon">Enter Nodemon</h2>
<p><strong>Nodemon restarts the server automatically</strong> when you save a file that’s used by the <code>server.js</code>. We can install Nodemon with the following command:</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash">$ <span class="token function">npm</span> <span class="token function">install</span> nodemon --save-dev
</code></pre><div class="toolbar"></div></div>
<div class="note"><p>We use a <code>--save-dev</code> flag here because we only use Nodemon when we are developing stuff. We won’t use Nodemon on an actual server. <code>--save-dev</code> here adds Nodeman as a <code>devDependency</code> in the <code>package.json</code> file.</p>
</div><figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/nodemon-dev-dep.png" alt="Saved Nodemon as a dev dependency.">
</figure>
<p>Nodemod behaves like Node. So you can run <code>nodemon server.js</code> and you’d expect to see the same thing. Unfortunately, this only works if you’ve installed nodemon globally with the <code>-g</code> flag (and we didn’t do this).</p>
<p>We have other ways to run Nodemon. For example, you can execute Nodemon directly from the <code>node_modules</code> folder. This is super unweildy, but it works:</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash">./node_modules/.bin/nodemon server.js
</code></pre><div class="toolbar"></div></div>
<p>We can make things simpler by adding <code>&nbsp;script</code> key in the <code>package.json</code> file. This lets us run <code>nodemon server.js</code> without the <code>./node_modules...</code> preamble.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token punctuation">{</span>
  <span class="token comment">// ...</span>
  <span class="token string">"scripts"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token string">"dev"</span><span class="token operator">:</span> <span class="token string">"nodemon server.js"</span>
  <span class="token punctuation">}</span>
  <span class="token comment">// ...</span>
<span class="token punctuation">}</span>
</code></pre><div class="toolbar"></div></div>
<p>Now, you can run <code>npm run dev</code> to trigger <code>nodemon server.js</code>.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/nodemon-server.png" alt="Uses npm run dev to run nodemon server.js">
</figure>
<p>Back to the main topic. We’re going to cover the <strong>CREATE</strong> operation next.</p>
<h2 id="crud---create">CRUD - CREATE</h2>
<p>Browsers can only perform a <strong>CREATE</strong> operation if they send <strong>POST</strong>  request to the server. This <code>POST</code> request can be triggered through JavaScript or through a <code>&lt;form&gt;</code> element.</p>
<p>Let’s figure out how to use a <code>&lt;form&gt;</code> element to 
create new entries for this Star Wars quote application for now. We’ll 
examine how to send requests via JavaScript later.</p>
<p>To send a POST request through a <code>&lt;form&gt;</code>, you need to add the <code>&lt;form&gt;</code> element to your <code>index.html</code> file.</p>
<p>You need three things on this form element:</p>
<ol>
<li>An <code>action</code> attribute</li>
<li>A <code>method</code> attribute</li>
<li><code>name</code> attributes on each <code>&lt;input&gt;</code> elements within the form</li>
</ol>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>form</span> <span class="token attr-name">action</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>/quotes<span class="token punctuation">"</span></span> <span class="token attr-name">method</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>POST<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>name<span class="token punctuation">"</span></span> <span class="token attr-name">name</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>name<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>quote<span class="token punctuation">"</span></span> <span class="token attr-name">name</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>quote<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>submit<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Submit<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>form</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<p>The <code>method</code> tells browsers what kind of request to send. In this case, we use <code>POST</code> because we’re sending a <code>POST</code> request.</p>
<p>The <code>action</code> attribute tells the browser where to send the <code>POST</code> request. In this case, we’re send the <code>POST</code> request to <code>/quotes</code>.</p>
<p>We can handle this <code>POST</code> request with a <code>post</code> method in <code>server.js</code>. The <code>path</code> path should be the value you placed in the <code>action</code> attribute.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">post</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'Hellooooooooooooooooo!'</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Restart your server (hopefully you’ve set up Nodemon so it restarts 
automatically) and refresh your browser. Then, enter something into the <code>&lt;form&gt;</code> element and submit the form. Next, look at your command line. You should see <code>Hellooooooooooooooooo!</code> in your command line.</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/post-log.png" alt="Logs helloooo in the command line.">
</figure>
<p>Great, we know that Express is handling the form for us right now. 
The next question is, how do we get the input values with Express?</p>
<p>Turns out, Express doesn’t handle reading data from the <code>&lt;form&gt;</code> element on it’s own. We have to add another package called <a href="https://www.npmjs.com/package/body-parser">body-parser</a> to gain this functionality.</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">npm</span> <span class="token function">install</span> body-parser --save
</code></pre><div class="toolbar"></div></div>
<p>Body-parser is a <strong>middleware</strong>. They help to tidy up the <code>request</code> object before we use them. Express lets us use middleware with the <code>use</code> method.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token keyword">const</span> express <span class="token operator">=</span> <span class="token function">require</span><span class="token punctuation">(</span><span class="token string">'express'</span><span class="token punctuation">)</span>
<span class="token keyword">const</span> bodyParser<span class="token operator">=</span> <span class="token function">require</span><span class="token punctuation">(</span><span class="token string">'body-parser'</span><span class="token punctuation">)</span>
<span class="token keyword">const</span> app <span class="token operator">=</span> <span class="token function">express</span><span class="token punctuation">(</span><span class="token punctuation">)</span>

<span class="token comment">// Make sure you place body-parser before your CRUD handlers!</span>
app<span class="token punctuation">.</span><span class="token function">use</span><span class="token punctuation">(</span>bodyParser<span class="token punctuation">.</span><span class="token function">urlencoded</span><span class="token punctuation">(</span><span class="token punctuation">{</span> extended<span class="token operator">:</span> <span class="token boolean">true</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">)</span>

<span class="token comment">// All your handlers here...</span>
app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/*...*/</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
app<span class="token punctuation">.</span><span class="token function">post</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/*...*/</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>The <code>urlencoded</code> method within body-parser tells body-parser to extract data from the <code>&lt;form&gt;</code> element and add them to the <code>body</code> property in the <code>request</code> object.</p>
<p>You should be able to see values from the <code>&lt;form&gt;</code> element inside  <code>req.body</code>  now. Try doing a <code>console.log</code> and see what it is!</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">post</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>req<span class="token punctuation">.</span>body<span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>You should see an object similar to the following:</p>
<figure><img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/post-log-req-body.png" alt="Logs the request body."></figure>
<p>Hmmm.</p>
<p>Master Yoda has spoken! Let’s make sure we remember Yoda’s words. 
It’s important. We want to be able to retrieve it the next time we load 
our index page.</p>
<p>Enter the database, MongoDB.</p>
<h2 id="mongodb">MongoDB</h2>
<p>MongoDB is a database. We can store information into this database to
 remember Yoda’s words. Then, we can retrieve this information and 
display to people who view our app.</p>
<div class="note"><p>I normally use <a href="https://mongoosejs.com/">Mongoose</a>
 (which is a framework for MongoDB) when I use MongoDB. I’ll teach you 
how to use basic MongoDB in this article. If you want to learn Mongoose,
 consider reading <a href="https://zellwk.com/blog/mongoose">my article on Mongoose</a>.</p>
</div><p>First, we need to install MongoDB via npm.</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">npm</span> <span class="token function">install</span> mongodb --save
</code></pre><div class="toolbar"></div></div>
<p>Once installed, we can connect to MongoDB through the <code>MongoClient</code>'s connect method as shown in the code below:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token keyword">const</span> MongoClient <span class="token operator">=</span> <span class="token function">require</span><span class="token punctuation">(</span><span class="token string">'mongodb'</span><span class="token punctuation">)</span><span class="token punctuation">.</span>MongoClient
</code></pre><div class="toolbar"></div></div>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span><span class="token string">'mongodb-connection-string'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">err<span class="token punctuation">,</span> client</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token comment">// ... do something here</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>The next part is to get the correct link to our database. Most people store their databases on cloud services like <a href="https://www.mongodb.com/cloud/atlas">MongoDB Atlas</a>. We’re going to do same as well. (It’s free).</p>
<div class="note"><p>You can also create a database on your computer for development work. Read “<a href="https://zellwk.com/blog/local-mongodb/">How to setup a local MongoDB Connection</a>” for instructions.</p>
</div><h3 id="setting-up-mongodb-atlas">Setting up MongoDB Atlas</h3>
<p>Go ahead and <a href="https://www.mongodb.com/download-center">create an account on MongoDB Atlas</a>.
 Once you’re done,  you need to create an “Organization”. It’s sort of 
like a company name. You can name it anything you want. (You can change 
it later).</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-create-org.png" alt="Creates an Atlas Organization">
</figure>
<p>You also need to select a cloud service. Go ahead with MongoDB Atlas in this case.</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-select-cloud.png" alt="Selects a cloud service.">
</figure>
<p>Next, you need to set permissions for users. MongoDB Atlas will 
automatically fill up your current email address as the user. So just 
continue to the next step.</p>
<p>You should end up with a screen that looks like this:</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-projects.png" alt="Atlas project screen.">
</figure>
<p>Next, you need to create a Database in MongoDB Atlas. There are several steps to do this.</p>
<p>First, you need to create a new Project. You can do this by going 
under “Context” in the top left hand menu. Click the Dropdown. Then, 
select New Project.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cp.png" alt="Creating a new project.">
</figure>
<p>Next, you will need to name your project. Call it anything you want. I’m going to call this <code>star-wars</code>.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cp-name-project.png" alt="Naming the project.">
</figure>
<p>Then, you will need to add members. Again, you’re already added so go ahead and click “Create Project” to move on.</p>
<p>You should end up with a screen that says Create a Cluster.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cluster.png" alt="Create Cluster Screen.">
</figure>
<p>Click on “Build a Cluster”. You should see this screen:</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cc.png" alt="Selecting a cluster">
</figure>
<p>Select the free cluster (left option) and continue. You should now 
see a screen to configure a cluster. Scroll down. Make sure you see 
these two things:</p>
<ol>
<li>Cluster Tier is M0 Sandbox</li>
<li>Monthly Estimate is FREE</li>
</ol>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cc-m0.png" alt="Configured the cluster.">
</figure>
<p>Click on Create cluster next. You should see “Your cluster is being created”.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cc-creating.png" alt="Creating cluster.">
</figure>
<p>You have to wait for approximately 5 minutes for the cluster creation. When the cluster is ready, you’ll see this:</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-cc-ready.png" alt="Cluster ready">
</figure>
<p>Now, we need to connect our Star Wars app with this cluster.</p>
<h3 id="connecting-to-mongodb-atlas">Connecting to MongoDB Atlas</h3>
<p>Click on the Connect button.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-connect.png" alt="Connect button.">
</figure>
<p>A modal should pop up.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-connect-modal.png" alt="Connection modal.">
</figure>
<p>You need to whitelist your IP address before you can connect to your 
cluster. This is a security feature built into MongoDB Atlas. Go ahead 
and click “Add your Current IP Address”.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-connect-add-ip.png" alt="Adds IP address.">
</figure>
<p>Next, you need to create a MongoDB user. This username and password 
is different from the one you used to login to MongoDB Atlas. This 
username and password is used ONLY for the database.</p>
<p>Make sure you remember MongoDB user and password. We’ll use it to connect to the database.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-connect-mongodb-user.png" alt="MongoDB User.">
</figure>
<p>Next, click on choose your connection method. Select “Connect to your application” and copy the connection string.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-connect-method.png" alt="Choosing a connection method.">
</figure>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-connection-string.png" alt="Copy the connection string.">
</figure>
<p>The connection string should look something like this:</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token string">'mongodb+srv://&lt;username&gt;:&lt;password&gt;@&lt;clustername&gt;-rmp3c.mongodb.net/test?retryWrites=true&amp;w=majority'</span>
</code></pre><div class="toolbar"></div></div>
<p>You need to replace 2 things here:</p>
<ol>
<li>Replace <code>&lt;username&gt;</code> with your Database username</li>
<li>Replace <code>&lt;password&gt;</code> with the Database user’s password</li>
</ol>
<div class="note"><p>The <code>test</code> in the connection string points to a <code>test</code> database. You would need to replace <code>test</code> with the name of your database if you use Mongoose. You can leave it as <code>test</code> if you use MongoClient like what we’re doing in this tutorial.</p>
</div><p>Put this connection string inside the <code>MongoClient.connect</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span>connectionString<span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">err<span class="token punctuation">,</span> client</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token comment">// ... do something here</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>We know we’ve connected to the database if there are no errors. Let’s create <code>console.log</code> statement that says “Connected to database”. This will help us know we’ve connected to the database when we restart the server.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span>connectionString<span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">err<span class="token punctuation">,</span> client</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span>err<span class="token punctuation">)</span> <span class="token keyword">return</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>err<span class="token punctuation">)</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'Connected to Database'</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>You should see something like this:</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/connected.png" alt="Connected to database.">
</figure>
<p>You can remove the deprecation warning by adding the option into <code>MongoClient.connect</code></p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span>connectionString<span class="token punctuation">,</span> <span class="token punctuation">{</span>
  useUnifiedTopology<span class="token operator">:</span> <span class="token boolean">true</span>
<span class="token punctuation">}</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">err<span class="token punctuation">,</span> client</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span>err<span class="token punctuation">)</span> <span class="token keyword">return</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>err<span class="token punctuation">)</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'Connected to Database'</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/connect-no-deprecation-warnings.png" alt="Connection without deprecation warning.">
</figure>
<p>MongoDB supports promises. If you want to use promises instead of callbacks, you can write <code>MongoClient.connect</code> like this. It behaves exactly like the code above.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span>connectionString<span class="token punctuation">,</span> <span class="token punctuation">{</span> useUnifiedTopology<span class="token operator">:</span> <span class="token boolean">true</span> <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">client</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'Connected to Database'</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<div class="note"><p>Read <a href="https://zellwk.com/blog/js-promises/" title="Promises in JavaScript">this article</a> if you want to learn about promises in JavaScript.</p>
</div><h3 id="changing-the-database">Changing the Database</h3>
<p>We need to change the database from <code>test</code> to something else. You can name it anything you want. I chose name my new database <code>star-wars-quotes</code> because it helps me remember what I’m building.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span>connectionString<span class="token punctuation">,</span> <span class="token punctuation">{</span> useUnifiedTopology<span class="token operator">:</span> <span class="token boolean">true</span> <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">client</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">'Connected to Database'</span><span class="token punctuation">)</span>
    <span class="token keyword">const</span> db <span class="token operator">=</span> client<span class="token punctuation">.</span><span class="token function">db</span><span class="token punctuation">(</span><span class="token string">'star-wars-quotes'</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<h3 id="mongodb-and-server">MongoDB and Server</h3>
<p>We need the <code>db</code> variable from the connection to to access
 MongoDB. This means we need to put our express request handlers into 
the MongoClient’s <code>then</code> call.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">client</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token comment">// ...</span>
    <span class="token keyword">const</span> db <span class="token operator">=</span> client<span class="token punctuation">.</span><span class="token function">db</span><span class="token punctuation">(</span><span class="token string">'star-wars-quotes'</span><span class="token punctuation">)</span>
    app<span class="token punctuation">.</span><span class="token function">use</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    app<span class="token punctuation">.</span><span class="token function">post</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    app<span class="token punctuation">.</span><span class="token function">listen</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span>console<span class="token punctuation">.</span>error<span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>We can finally store Yoda’s quote into the database now!</p>
<h2 id="crud---create-(continued)">CRUD - CREATE (continued)</h2>
<p>We need to create a <code>collection</code> before we can store items into a database. Here’s a simple analogy to help you clear up the terms in MongoDB:</p>
<ul>
<li>Imagine a Database is a Room.</li>
<li>A Room contains boxes (<code>collections</code>).</li>
</ul>
<p>Like Databases, you can name collections anything you want. In this case, let’s store quotes into a <code>quotes</code> collection. We use <code>db.collection</code> to specify the collection.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">MongoClient<span class="token punctuation">.</span><span class="token function">connect</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">client</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token comment">// ...</span>
    <span class="token keyword">const</span> db <span class="token operator">=</span> client<span class="token punctuation">.</span><span class="token function">db</span><span class="token punctuation">(</span><span class="token string">'star-wars-quotes'</span><span class="token punctuation">)</span>
    <span class="token keyword">const</span> quotesCollection <span class="token operator">=</span> db<span class="token punctuation">.</span><span class="token function">collection</span><span class="token punctuation">(</span><span class="token string">'quotes'</span><span class="token punctuation">)</span>

    <span class="token comment">// ...</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>We can use the <code>insertOne</code> method to add items into a MongoDB collection.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">post</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">insertOne</span><span class="token punctuation">(</span>req<span class="token punctuation">.</span>body<span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>result<span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Try submitting the <code>&lt;form&gt;</code> from the browser. You should see a big scary looking <code>result</code> in the Terminal.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/post-result.png" alt="Post result.">
</figure>
<p>If you see this, congratulations! You’ve successfully add the quote into the database.</p>
<p>You can check the items inside the database by going to “Collections” in MongoDB Atlas.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-collections.png" alt="Collections in MongoDB Atlas.">
</figure>
<p>You should see a document in your database. (Each database entry is called a document).</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/atlas-collection-view.png" alt="Documents in MongoDB Atlas.">
</figure>
<p>If you go back to the Browser, you’ll see it’s still trying to load something.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/post-loading.png" alt="Browser still trying to load a page.">
</figure>
<p>This happens because the browser expects something back from the server.</p>
<p>In this case, we don’t need to send the browser information. Let’s ask the browser to redirect back to <code>/</code> instead. We do this with <code>res.redirect</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">post</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">insertOne</span><span class="token punctuation">(</span>req<span class="token punctuation">.</span>body<span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      res<span class="token punctuation">.</span><span class="token function">redirect</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/post-redirected.png" alt="Redirected. Browser is no longer waiting to load something.">
</figure>
<p>Yay!</p>
<p>Since we have some quotes in the collection, let’s show them to our user when they land on the page!</p>
<h2 id="showing-quotes-to-users-(read-operation)">Showing quotes to users (READ operation)</h2>
<p>We need to do two things to show quotes from MongoDB Atlas to our users.</p>
<ol>
<li>Get quotes from MongoDB Atlas.</li>
<li>Rendering the quotes in HTML with a template engine</li>
</ol>
<p>Let’s go one step at a time.</p>
<h3 id="getting-quotes-from-mongodb">Getting quotes from MongoDB</h3>
<p>We can get quotes we stored in MongoDB with the <code>find</code> method. This method from mLab by using the <code>find</code> method that’s available in the <code>collection</code> method.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token keyword">const</span> cursor <span class="token operator">=</span> db<span class="token punctuation">.</span><span class="token function">collection</span><span class="token punctuation">(</span><span class="token string">'quotes'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">find</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>cursor<span class="token punctuation">)</span>
  <span class="token comment">// ...</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>The <code>find</code> method returns a <code>cursor</code> which won’t make sense if you tried logging it.</p>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/cursor.png" alt="Cursor object from MongoDB">
</figure>
<p>But this <code>cursor</code> object contains all quotes from our database! It has a bunch of method that lets us get our data. For example, we can use <code>toArray</code> to convert the data into an array.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  db<span class="token punctuation">.</span><span class="token function">collection</span><span class="token punctuation">(</span><span class="token string">'quotes'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">find</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">toArray</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">results</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>results<span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
  <span class="token comment">// ...</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<figure>
&nbsp; <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/mongodb-get.png" alt="">
</figure>
<p>Great! We see the quotes we added! (You see so many of the same quotes because I added them all when writing this tutorial 😆).</p>
<p>Next we want to generate a HTML that contains all our quotes.</p>
<h3 id="rendering-the-html">Rendering the HTML</h3>
<p>We cannot serve up the <code>index.html</code> file and expect quotes to magically appear because there’s no way to add dynamic content to a HTML file.</p>
<p>What we can do, instead, is to use a template engine to generate the HTML. Popular template engines include <a href="https://pugjs.org/api/getting-started.html" title="Pug">Pug</a>, <a href="https://ejs.co/" title="Embedded JavaScript (EJS)">Embedded JavaScript</a>, and <a href="https://mozilla.github.io/nunjucks/" title="Nunjucks">Nunjucks</a>.</p>
<div class="note"><p>I’ve wrote extensively about the how and why of template engines in a <a href="https://zellwk.com/blog/nunjucks-with-gulp/">separate post</a>. You might want to check it out if you have no idea what template engines are.</p>
<p>I use Nunjucks as my template engine of choice. Feel free to check out the post to find out why.</p>
</div><p>For this tutorial, we will use <a href="http://www.embeddedjs.com/">Embedded JavaScript</a>
 (EJS) as our template engine because it’s the easiest to start with. 
You’ll find it familiar from the get-go since you’ll be writing HTML and
 JavaScript.</p>
<h3 id="using-ejs">Using EJS</h3>
<p>First, we need to install EJS.</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">npm</span> <span class="token function">install</span> ejs --save
</code></pre><div class="toolbar"></div></div>
<p>Next, we need to set <code>view engine</code> to <code>ejs</code>. This tells Express we’re using EJS as the template engine. You can need to place it before any <code>app.use</code>, <code>app.get</code> or <code>app.post</code> methods.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">set</span><span class="token punctuation">(</span><span class="token string">'view engine'</span><span class="token punctuation">,</span> <span class="token string">'ejs'</span><span class="token punctuation">)</span>

<span class="token comment">// Middlewares and other routes here...</span>
</code></pre><div class="toolbar"></div></div>
<p><strong>We can now generate HTML that contains the quotes</strong>. This process is called <strong>rendering</strong> the HTML.</p>
<p>We will use the <code>render</code> method built into Express’s <code>response</code>. It needs to follow the following syntax:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">res<span class="token punctuation">.</span><span class="token function">render</span><span class="token punctuation">(</span>view<span class="token punctuation">,</span> locals<span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<ul>
<li><code>view</code> is the name of the file we’re rendering. This file must be placed inside a <code>views</code> folder.</li>
<li><code>locals</code>is the data passed into the file.</li>
</ul>
<p>Let’s create a view. We’ll make an <code>index.ejs</code> file inside the views folder.</p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash"><span class="token function">mkdir</span> views
<span class="token function">touch</span> views/index.ejs
</code></pre><div class="toolbar"></div></div>
<p>We’ll copy/paste everything from <code>index.html</code> into <code>index.ejs</code>.</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token comment">&lt;!-- index.ejs --&gt;</span>
<span class="token doctype">&lt;!DOCTYPE html&gt;</span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>html</span> <span class="token attr-name">lang</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>en<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>head</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>meta</span> <span class="token attr-name">charset</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>UTF-8<span class="token punctuation">"</span></span> <span class="token punctuation">/&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>title</span><span class="token punctuation">&gt;</span></span>Star Wars Quote App<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>title</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>head</span><span class="token punctuation">&gt;</span></span>

  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>body</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h1</span><span class="token punctuation">&gt;</span></span>May Node and Express be with you.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h1</span><span class="token punctuation">&gt;</span></span>

    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>form</span> <span class="token attr-name">action</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>/quotes<span class="token punctuation">"</span></span> <span class="token attr-name">method</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>POST<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>name<span class="token punctuation">"</span></span> <span class="token attr-name">name</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>name<span class="token punctuation">"</span></span> <span class="token punctuation">/&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>input</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>quote<span class="token punctuation">"</span></span> <span class="token attr-name">name</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>quote<span class="token punctuation">"</span></span> <span class="token punctuation">/&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>submit<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Submit<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>form</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>body</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>html</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<p>Next, we’ll use <code>res.render</code> to render this <code>index.ejs</code> file.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  db<span class="token punctuation">.</span><span class="token function">collection</span><span class="token punctuation">(</span><span class="token string">'quotes'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">find</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">toArray</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
  res<span class="token punctuation">.</span><span class="token function">render</span><span class="token punctuation">(</span><span class="token string">'index.ejs'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>If you refresh the page, you should still see the same thing. Nothing should change, nothing should break.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/get-ejs.png" alt="Renders an ejs file.">
</figure>
<p>Let’s put the quotes into <code>index.ejs</code>. To do this, we need to pass the quotes into the <code>render</code> method.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">get</span><span class="token punctuation">(</span><span class="token string">'/'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  db<span class="token punctuation">.</span><span class="token function">collection</span><span class="token punctuation">(</span><span class="token string">'quotes'</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">find</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">toArray</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">results</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      res<span class="token punctuation">.</span><span class="token function">render</span><span class="token punctuation">(</span><span class="token string">'index.ejs'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> quotes<span class="token operator">:</span> results <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>In <code>index.ejs</code>, we can use place variables between <code>&lt;%=</code> and <code>%&gt;</code> tags. Let’s try putting <code>quotes</code> into the HTML:</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token comment">&lt;!-- In index.ejs --&gt;</span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>body</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h1</span><span class="token punctuation">&gt;</span></span> ... <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h1</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>form</span><span class="token punctuation">&gt;</span></span> ... <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>form</span><span class="token punctuation">&gt;</span></span>
  &lt;%= quotes %&gt;
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>body</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<p>You should see this:</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/ejs-quotes.png" alt="Rendered quotes in EJS.">
</figure>
<p>We see lots of <code>[object Object]</code> because each quote inside <code>results</code> is a JavaScript object. <code>ejs</code> cannot convert that object into HTML automatically.</p>
<p>We need to loop through the quotes. We can do this with a <code>for</code> loop. In EJS, we write a for loop like how we write a JavaScript <code>for</code> loop. The only difference is we need to put the <code>for</code> loop statements between <code>&lt;%</code> and <code>%&gt;</code>.</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h2</span><span class="token punctuation">&gt;</span></span> Quotes <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h2</span><span class="token punctuation">&gt;</span></span>

<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>ul</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>quotes<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token comment">&lt;!-- Loop through quotes --&gt;</span>
  &lt;% for(var i = 0; i &lt; quotes.length; i++) {%&gt;
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>quote<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
      <span class="token comment">&lt;!-- Output name from the iterated quote object --&gt;</span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span><span class="token punctuation">&gt;</span></span>&lt;%= quotes[i].name %&gt;<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span>:
      <span class="token comment">&lt;!-- Output quote from the iterated quote object --&gt;</span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span><span class="token punctuation">&gt;</span></span>&lt;%= quotes[i].quote %&gt;<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
  &lt;% } %&gt;
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>ul</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/ejs-rendered-quotes.png" alt="Rendered quotes with EJS.">
</figure>
<h2 id="crud---update">CRUD - UPDATE</h2>
<p>We use the <strong>UPDATE</strong> operation when we want to change something. It can be triggered with a <strong>PUT</strong> request. Like <code>POST</code>, <code>PUT</code> can be triggered either through JavaScript or through a <code>&lt;form&gt;</code> element.</p>
<p>Let’s switch things up and use JavaScript since you already know how to use <code>&lt;form&gt;</code> elements.</p>
<p>For this update operation, we will create a button that replaces the first quote by Yoda to something written by Darth Vadar.</p>
<p>To do this, we need to add a <code>button</code> into the <code>index.ejs</code> file:</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h2</span><span class="token punctuation">&gt;</span></span>Darth Vadar invades!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h2</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>
    Replace first Yoda's quote with a quote written by Darth Vadar
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>update-button<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Replace Yoda's quote<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/put-ejs.png" alt="Add update section to HTML.">
</figure>
<p>We will also create an external JavaScript file to execute a <code>PUT</code> request. According to Express conventions, this JavaScript is kept in a folder called <code>public</code></p>
<div class="code-toolbar"><pre class=" language-bash"><code class=" language-bash">$ <span class="token function">mkdir</span> public
$ <span class="token function">touch</span> public/main.js
</code></pre><div class="toolbar"></div></div>
<p>Then, we have to tell Express to make this <code>public</code> folder accessible to the public by using a built-in middleware called <code>express.static</code></p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">use</span><span class="token punctuation">(</span>express<span class="token punctuation">.</span><span class="token function">static</span><span class="token punctuation">(</span><span class="token string">'public'</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>We now can add the <code>main.js</code> file to the <code>index.ejs</code> file:</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>body</span><span class="token punctuation">&gt;</span></span>
  <span class="token comment">&lt;!-- ... --&gt;</span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>script</span> <span class="token attr-name">src</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>/main.js<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token script"></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>script</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>body</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<p>We will send a <code>PUT</code> request when the button gets clicked. This means we need to listen to a <code>click</code> event.</p>
<p>Next, we’re going to send the <strong>PUT</strong> request when the button is clicked:</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token comment">// main.js</span>
<span class="token keyword">const</span> update <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">querySelector</span><span class="token punctuation">(</span><span class="token string">'#update-button'</span><span class="token punctuation">)</span>

update<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token comment">// Send PUT Request here</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<h3 id="sending-a-put-request">Sending a PUT Request</h3>
<p>The easiest way to trigger a <strong>PUT</strong> request in modern browsers is to use the <a href="https://developer.mozilla.org/en/docs/Web/API/Fetch_API">Fetch API</a>.</p>
<p>Fetch has the following syntax:</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token function">fetch</span><span class="token punctuation">(</span>endpoint<span class="token punctuation">,</span> options<span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>In this case, let’s say we want to send the request to <code>/quotes</code>. We’ll set <code>endpoint</code> to <code>/quotes</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">update<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>We need to send a <code>PUT</code> request this time. We can do this by setting Fetch’s method to <code>put</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">update<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'put'</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Modern applications send JSON data to servers. They also receive JSON
 data back to servers. JSON stands for JavaScript Object Notation. 
They’re like JavaScript objects, but each property and value are written
 between two quotation marks.</p>
<p>Here’s an example of JavaScript data:</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token keyword">const</span> data <span class="token operator">=</span> <span class="token punctuation">{</span>
  name<span class="token operator">:</span> <span class="token string">'Darth Vadar'</span><span class="token punctuation">,</span>
  quote<span class="token operator">:</span> <span class="token string">'I find your lack of faith disturbing.'</span>
<span class="token punctuation">}</span>
</code></pre><div class="toolbar"></div></div>
<p>And what its JSON counterpart looks like. (Notice how everything is wrapped between two <code>"</code>).</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token punctuation">{</span>
  <span class="token string">"name"</span><span class="token operator">:</span> <span class="token string">"Darth Vadar"</span><span class="token punctuation">,</span>
  <span class="token string">"quote"</span><span class="token operator">:</span> <span class="token string">"I find your lack of faith disturbing."</span>
<span class="token punctuation">}</span>
</code></pre><div class="toolbar"></div></div>
<p>We need to tell the server we’re sending JSON data by setting the <code>Content-Type</code> headers to <code>application/json</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">update<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'put'</span><span class="token punctuation">,</span>
    headers<span class="token operator">:</span> <span class="token punctuation">{</span> <span class="token string">'Content-Type'</span><span class="token operator">:</span> <span class="token string">'application/json'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Next, we need to convert the data we send into JSON. We can do this with <code>JSON.stringify</code>. This data is passed via the <code>body</code> property.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">update<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'put'</span><span class="token punctuation">,</span>
    headers<span class="token operator">:</span> <span class="token punctuation">{</span> <span class="token string">'Content-Type'</span><span class="token operator">:</span> <span class="token string">'application/json'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token constant">JSON</span><span class="token punctuation">.</span><span class="token function">stringify</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
      name<span class="token operator">:</span> <span class="token string">'Darth Vadar'</span><span class="token punctuation">,</span>
      quote<span class="token operator">:</span> <span class="token string">'I find your lack of faith disturbing.'</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<h3 id="accepting-the-put-request">Accepting the PUT request</h3>
<p>Our server doesn’t accept JSON data yet. We can teach it to read JSON by adding the <code>body-parser</code>'s <code>json</code> middleware.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">use</span><span class="token punctuation">(</span>bodyParser<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Next, we can handle the <code>PUT</code> request with a <code>put</code> method. You should be able to see the values we send from the fetch request.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">put</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>req<span class="token punctuation">.</span>body<span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/put-body.png" alt="Put request body">
</figure>
<p>The next step is to change the Yoda’s first quote to this quote by Darth Vadar.</p>
<h3 id="changing-yoda%E2%80%99s-quote">Changing Yoda’s quote</h3>
<p>MongoDB Collections come with a method called <code>findOneAndUpdate</code>. This method lets us find and change one item in the database. It has the following syntax:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">quotesCollection<span class="token punctuation">.</span><span class="token function">findOneAndUpdate</span><span class="token punctuation">(</span>
  query<span class="token punctuation">,</span>
  update<span class="token punctuation">,</span>
  options
<span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p><code>query</code> lets us filter the collection with key-value pairs. If we want to filter quotes to those written by Yoda, we can set <code>{ name: 'Yoda' }</code> as the query.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">quotesCollection<span class="token punctuation">.</span><span class="token function">findOneAndUpdate</span><span class="token punctuation">(</span>
  <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">'Yoda'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  update<span class="token punctuation">,</span>
  options
<span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p><code>update</code>, tells MongoDB what to change. It uses MongoDB’s <a href="https://docs.mongodb.org/manual/reference/operator/update/">update operators</a> like <code>$set</code>, <code>$inc</code> and <code>$push</code>.</p>
<p>We will use the <code>$set</code> operator since we’re changing Yoda’s quotes into Darth Vadar’s quotes:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">quotesCollection<span class="token punctuation">.</span><span class="token function">findOneAndUpdate</span><span class="token punctuation">(</span>
  <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">'Yoda'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    $<span class="token keyword">set</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      name<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>name<span class="token punctuation">,</span>
      quote<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>quote
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  options
<span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p><code>options</code> tells MongoDB to define additional options for this update request.</p>
<p>In this case, it’s possible that no Yoda quotes exist in the 
database. We can force MongoDB to create a new Darth Vadar quote if no 
Yoda quotes exist. We do this by setting <code>upsert</code> to <code>true</code>. <code>upsert</code> means: Insert a document if no documents can be updated.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">quotesCollection<span class="token punctuation">.</span><span class="token function">findOneAndUpdate</span><span class="token punctuation">(</span>
  <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">'Yoda'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    $<span class="token keyword">set</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      name<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>name<span class="token punctuation">,</span>
      quote<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>quote
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    upsert<span class="token operator">:</span> <span class="token boolean">true</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Finally, let’s log the <code>result</code> into the command line.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">put</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">findOneAndUpdate</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>result<span class="token punctuation">)</span>
     <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span>
</code></pre><div class="toolbar"></div></div>
<p>Try clicking the “replace first Yoda quote” button in the browser. 
You should see this result in your command line. This says we changed 
one of Yoda’s quote.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/put-result.png" alt="Put result.">
</figure>
<p>If you refresh the browser, you should see Darth Vadar’s quote as the first quote.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/put-check.png" alt="Checking the HTML after a PUT request.">
</figure>
<div class="note"><p>Does the <code>findOneAndUpdate</code> look 
complicated to you? Well, It IS complicated. This is why I use Mongoose 
instead of MongoDB. You can find out more about Mongoose in <a href="https://zellwk.com/blog/mongoose" title="Mongoose 101">this article</a>.</p>
</div><p>Finally, we need to respond to the JavaScript that sent the <code>PUT</code> request. In this case, we’ll simply send the <code>success</code> message.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">put</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">findOneAndUpdate</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
       res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token string">'Success'</span><span class="token punctuation">)</span>
     <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span>
</code></pre><div class="toolbar"></div></div>
<p>Next, we can handle the response from the server via a <code>then</code> object. (We do this because <code>fetch</code> returns a promise). However, Fetch is slightly different from most promises. You need to use another <code>then</code> object to get the response from the server.</p>
<p>Here’s what you should do:</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token punctuation">{</span> <span class="token comment">/* request */</span> <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">res</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token keyword">if</span> <span class="token punctuation">(</span>res<span class="token punctuation">.</span>ok<span class="token punctuation">)</span> <span class="token keyword">return</span> res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">response</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>response<span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>You should be able to see a <code>Success</code> message from the server in the console.</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/put-response.png" alt="Response from the server.">
</figure>
<div class="note"><p>I wrote an <a href="https://css-tricks.com/using-fetch/">article on the Fetch API</a> if you’re wondering why we need two <code>then</code> calls. Give it a read! It’ll help cement your understanding.</p>
</div><p>If you are working on a fancy webapp, you can use JavaScript to update the DOM, so users see the new changes immediately.</p>
<p>However, updating the DOM is out of scope of this article, so we’re just going to refresh the browser to see the changes.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript"><span class="token function">fetch</span><span class="token punctuation">(</span><span class="token punctuation">{</span> <span class="token comment">/* request */</span> <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">res</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token keyword">if</span> <span class="token punctuation">(</span>res<span class="token punctuation">.</span>ok<span class="token punctuation">)</span> <span class="token keyword">return</span> res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">response</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    window<span class="token punctuation">.</span>location<span class="token punctuation">.</span><span class="token function">reload</span><span class="token punctuation">(</span><span class="token boolean">true</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<div class="note"><p>If you want to learn to use JavaScript to update the DOM, I suggest going through my <a href="https://learnjavascript.today/">Learn JavaScript</a> course. I even teach you how make your interface fast and snappy! (Check the Todolist Component).</p>
</div><p>That’s it for the <strong>UPDATE</strong> operation! Let’s move on to delete.</p>
<h2 id="crud---delete">CRUD - DELETE</h2>
<p>The <strong>DELETE</strong> operation can be triggered through a <strong>DELETE</strong> request. It’s similar to the <code>UPDATE</code> request so this should be simple if you understand what we’ve done above.</p>
<p>For this, let’s delete the first quote by Darth Vadar.</p>
<p>First, we need to add a delete button to <code>index.ejs</code>.</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h2</span><span class="token punctuation">&gt;</span></span>Remove Darth Vadar!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h2</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>
    Delete one Darth Vadar's quote. Does nothing if there are no more Darth
    Vadar's quote
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>delete-button<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Delete Darth Vadar's quote<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/delete-ejs.png" alt="Adds delete button to the index.ejs file.">
</figure>
<p>Then, we’ll trigger a <strong>DELETE</strong> request through Fetch when a user clicks the delete button.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token keyword">const</span> deleteButton <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">querySelector</span><span class="token punctuation">(</span><span class="token string">'#delete-button'</span><span class="token punctuation">)</span>

deleteButton<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'delete'</span><span class="token punctuation">,</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Since we’re deleting a quote by Darth Vadar, we only need to send Darth Vadar’s name to the server.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">deleteButton<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">,</span> <span class="token punctuation">{</span>
    method<span class="token operator">:</span> <span class="token string">'delete'</span><span class="token punctuation">,</span>
    headers<span class="token operator">:</span> <span class="token punctuation">{</span> <span class="token string">'Content-Type'</span><span class="token operator">:</span> <span class="token string">'application/json'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    body<span class="token operator">:</span> <span class="token constant">JSON</span><span class="token punctuation">.</span><span class="token function">stringify</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
      name<span class="token operator">:</span> <span class="token string">'Darth Vadar'</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">res</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      <span class="token keyword">if</span> <span class="token punctuation">(</span>res<span class="token punctuation">.</span>ok<span class="token punctuation">)</span> <span class="token keyword">return</span> res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">data</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      window<span class="token punctuation">.</span>location<span class="token punctuation">.</span><span class="token function">reload</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>We can then handle the event on our server side with the <code>delete</code> method:</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">delete</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token comment">// Handle delete event here</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<h3 id="deleting-a-document-from-mongodb">Deleting a document from MongoDB</h3>
<p>MongoDB Collections has a method called <code>deleteOne</code>. It lets us remove a document from the database. It takes in two parameters: <code>query</code> and <code>options</code>.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">quotesCollection<span class="token punctuation">.</span><span class="token function">deleteOne</span><span class="token punctuation">(</span>
  query<span class="token punctuation">,</span>
  options
<span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p><code>query</code> works like <code>query</code> in <code>findOneAndUpdate</code>. It lets us filter the collection to the entries we’re searching for. In this case, we can set <code>name</code> to Darth Vadar.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">quotesCollection<span class="token punctuation">.</span><span class="token function">deleteOne</span><span class="token punctuation">(</span>
  <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">'Darth Vadar'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  options
<span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span><span class="token comment">/* ... */</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>However, since we already pass the name <code>Darth Vadar</code> from Fetch, we don’t need to hardcode it in Express anymore. We can simply use <code>req.body.name</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">delete</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">deleteOne</span><span class="token punctuation">(</span>
    <span class="token punctuation">{</span> name<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>name <span class="token punctuation">}</span><span class="token punctuation">,</span>
    options
  <span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>In this case, we don’t need to change any options, so we can omit <code>options</code>.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js">app<span class="token punctuation">.</span><span class="token function">delete</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">deleteOne</span><span class="token punctuation">(</span>
    <span class="token punctuation">{</span> name<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>name <span class="token punctuation">}</span>
  <span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Then, we can send a response back to the JavaScript in the <code>then</code> call.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">delete</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">deleteOne</span><span class="token punctuation">(</span>
    <span class="token punctuation">{</span> name<span class="token operator">:</span> req<span class="token punctuation">.</span>body<span class="token punctuation">.</span>name <span class="token punctuation">}</span>
  <span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token template-string"><span class="token template-punctuation string">`</span><span class="token string">Deleted Darth Vadar's quote</span><span class="token template-punctuation string">`</span></span><span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>Now, when you click the delete button, the browser will sends <em>DELETE</em> request through Fetch to our Express server. Then, the server responds by sending either an error or a message back.</p>
<h3 id="what-if-there-are-no-more-darth-vadar-quotes%3F">What if there are no more Darth Vadar quotes?</h3>
<p>If there are no more Darth Vadar quotes, <code>result.deletedCount</code> will be <code>0</code>. We can send a message that says tells the browser that there are no more Darth Vadar quotes to delete.</p>
<div class="code-toolbar"><pre class=" language-javascript"><code class=" language-javascript">app<span class="token punctuation">.</span><span class="token function">delete</span><span class="token punctuation">(</span><span class="token string">'/quotes'</span><span class="token punctuation">,</span> <span class="token punctuation">(</span><span class="token parameter">req<span class="token punctuation">,</span> res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  quotesCollection<span class="token punctuation">.</span><span class="token function">deleteOne</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">result</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      <span class="token keyword">if</span> <span class="token punctuation">(</span>result<span class="token punctuation">.</span>deletedCount <span class="token operator">===</span> <span class="token number">0</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token keyword">return</span> res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token string">'No quote to delete'</span><span class="token punctuation">)</span>
      <span class="token punctuation">}</span>
      res<span class="token punctuation">.</span><span class="token function">json</span><span class="token punctuation">(</span><span class="token template-string"><span class="token template-punctuation string">`</span><span class="token string">Deleted Darth Vadar's quote</span><span class="token template-punctuation string">`</span></span><span class="token punctuation">)</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">error</span> <span class="token operator">=&gt;</span> console<span class="token punctuation">.</span><span class="token function">error</span><span class="token punctuation">(</span>error<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<p>If the JavaScript receives a <code>No quote to delete</code> response, we can tell the user there’s no Darth Vadar quote to delete.</p>
<p>To do this, let’s add an element where we can tell users about this message.</p>
<div class="code-toolbar"><pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>message<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
</code></pre><div class="toolbar"></div></div>
<p>If we receive <code>No quote to delete</code>, we can change the <code>textContent</code> of this <code>.message</code> div.</p>
<div class="code-toolbar"><pre class=" language-js"><code class=" language-js"><span class="token keyword">const</span> messageDiv <span class="token operator">=</span> document<span class="token punctuation">.</span><span class="token function">querySelector</span><span class="token punctuation">(</span><span class="token string">'#message'</span><span class="token punctuation">)</span>

deleteButton<span class="token punctuation">.</span><span class="token function">addEventListener</span><span class="token punctuation">(</span><span class="token string">'click'</span><span class="token punctuation">,</span> <span class="token parameter">_</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token function">fetch</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">response</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      <span class="token keyword">if</span> <span class="token punctuation">(</span>response <span class="token operator">===</span> <span class="token string">'No quote to delete'</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        messageDiv<span class="token punctuation">.</span>textContent <span class="token operator">=</span> <span class="token string">'No Darth Vadar quote to delete'</span>
      <span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
        window<span class="token punctuation">.</span>location<span class="token punctuation">.</span><span class="token function">reload</span><span class="token punctuation">(</span><span class="token boolean">true</span><span class="token punctuation">)</span>
      <span class="token punctuation">}</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span>
    <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token comment">/* ... */</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="toolbar"></div></div>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/delete-result.gif" alt="Message if no quotes can be deleted.">
</figure>
<p>That’s it for the <strong>DELETE</strong> operation!</p>
<h2 id="make-it-look-better%E2%80%A6">Make it look better…</h2>
<p>The final step is to make the app look a little better by sprinkling some styles!</p>
<figure role="figure">
  <img src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/looks.png" alt="Add CSS to make the app look better.">
</figure>
<h2 id="wrapping-up">Wrapping Up</h2>
<p>We covered A LOT in this mega tutorial. Here’s a list of things we’ve done together:</p>
<ol>
<li>Understood what Express, Node, and MongoDB are used for</li>
<li>Understood CRUD</li>
<li>Executed Create, Read, Update and Delete operations</li>
<li>Created an Atlas account for MongoDB</li>
<li>Save, read, update, and delete from MongoDB</li>
<li>Display variable data with template engines</li>
</ol>
<p>You have now learned all you need to know about creating simple 
applications with Node, Express, and MongoDB. Now, go forth and create 
more applications, young padawan. May the force be with you.</p>
<h2 id="grab-the-source-code">Grab the Source Code</h2>
<p>You can grab the source code by leaving your name and email address in <a href="#convertkit">this form</a>. I’ll also send you this article in PDF so you can read it at your leisure.</p>
<h2 id="further-reading">Further reading</h2>
<p>Here’s some further readings if you’re interested to continue with the Node, Express, MongoDB journey</p>
<ul>
<li>Express articles
<ul>
<li><a href="https://zellwk.com/blog/express-middlewares">3 useful Express middleware</a></li>
<li><a href="https://zellwk.com/blog/express-errors">Handling Express errors</a></li>
<li><a href="https://zellwk.com/blog/async-await">JavaScript Async/await</a></li>
<li><a href="https://zellwk.com/blog/async-await-express">Using Async/await in Express</a></li>
</ul>
</li>
<li>MongoDB articles
<ul>
<li><a href="https://zellwk.com/blog/mongoose">Mongoose 101</a></li>
</ul>
</li>
<li>Testing related articles
<ul>
<li><a href="https://zellwk.com/blog/endpoint-testing">Endpoint testing with Jest and Supertest</a></li>
<li><a href="https://zellwk.com/blog/jest-and-mongoose">Connecting Jest and Mongoose</a></li>
</ul>
</li>
</ul>
<p>If you enjoyed this article, please tell a friend about it! Share it on <a href="https://twitter.com/share?text=Building%20a%20Simple%20CRUD%20app%20with%20Node%2C%20Express%2C%20and%20MongoDB%20by%20@zellwk%20%F0%9F%91%87%20&amp;url=https://zellwk.com/blog/crud-express-mongodb/" target="_blank" rel="noopener">Twitter</a>. If you spot a typo, I’d appreciate if you can correct <a href="https://github.com/zellwk/zellwk.com/blob/master/src/posts/2020-04-09-crud-express-mongodb.md">it on GitHub</a>. Thank you!</p>

  </article>

  <div class="post-layout">
    <section id="convertkit" class="o-content c-ck ck-layout jsCkForm">


<div><div class="ck_form_container ck_inline" data-ck-version="6">
  
<div class="ck_form ck_vertical_subscription_form ck_horizontal">
  <div class="ck_form_content">
    <h3 class="ck_form_title">Get the Source code for this CRUD app</h3>
    <div class="ck_description">
      <span class="ck_image">
        
      </span>
      <p>Grab the Source Code so you can check your work! You’ll also get this article in PDF so you can read it offline at your leisure.</p>
    </div>
  </div>

  <div class="ck_form_fields">
    <div id="ck_success_msg" style="display:none;">
      <p>Success! Now check your email to confirm your subscription.</p>
    </div>

    <!--  Form starts here  -->
    <form id="ck_subscribe_form" class="ck_subscribe_form" action="https://forms.convertkit.com/landing_pages/466666/subscribe" data-remote="true">
      <input type="hidden" value="{&quot;form_style&quot;:&quot;full&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options">
      <input type="hidden" name="id" value="466666" id="landing_page_id">
      <input type="hidden" name="ck_form_recaptcha" value="" id="ck_form_recaptcha">
      <div class="ck_errorArea">
        <div id="ck_error_msg" style="display:none">
          <p>There was an error submitting your subscription. Please try again.</p>
        </div>
      </div>
      <div class="ck_control_group ck_first_name_field_group">
        <label class="ck_label" for="ck_firstNameField">First Name</label>
        <input type="text" name="first_name" class="ck_first_name" id="ck_firstNameField">
      </div>
      <div class="ck_control_group ck_email_field_group">
        <label class="ck_label" for="ck_emailField">Email Address</label>
        <input type="email" name="email" class="ck_email_address" id="ck_emailField" required="">
      </div>
      <div class="ck_control_group ck_captcha2_h_field_group ck-captcha2-h" style="position: absolute !important;left: -999em !important;">
        <label class="ck_label" for="ck_captcha2_h">We use this field to detect spam bots. If you fill this in, you will be marked as a spammer.</label>
        <input type="text" name="captcha2_h" class="ck-captcha2-h" id="ck_captcha2_h">
      </div>

        <label class="ck_checkbox" style="display:none;">
          <input class="optIn ck_course_opted" name="course_opted" type="checkbox" id="optIn" checked="checked">
          <span class="ck_opt_in_prompt">I'd like to receive the free email course.</span>
        </label>

      <button class="subscribe_button ck_subscribe_button btn fields" id="ck_subscribe_button">
        Get the source code
      </button>
      <span class="ck_guarantee">
        
          <a class="ck_powered_by" href="http://mbsy.co/clb2r">Powered by ConvertKit</a>
      </span>
    </form>
  </div>

</div>

</div>


<style type="text/css">/* Layout */
  .ck_form {
  /* divider image */
	background: #fff url(data:image/gif;base64,R0lGODlhAQADAIABAMzMzP///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS41LWMwMTQgNzkuMTUxNDgxLCAyMDEzLzAzLzEzLTEyOjA5OjE1ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUQ5NjM5RjgxQUVEMTFFNEJBQTdGNTQwMjc5MTZDOTciIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUQ5NjM5RjkxQUVEMTFFNEJBQTdGNTQwMjc5MTZDOTciPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxRDk2MzlGNjFBRUQxMUU0QkFBN0Y1NDAyNzkxNkM5NyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxRDk2MzlGNzFBRUQxMUU0QkFBN0Y1NDAyNzkxNkM5NyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAAAEALAAAAAABAAMAAAICRFIAOw==) repeat-y center top;
	font-family: "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif;
	line-height: 1.5em;
	overflow: hidden;
	color: #666;
	font-size: 16px;
	border-top: solid 20px #3071b0;
  border-top-color: #3071b0;
	border-bottom: solid 10px #3d3d3d;
  border-bottom-color: #1d446a;
	-webkit-box-shadow: 0px 0px 5px rgba(0,0,0,.3);
	-moz-box-shadow: 0px 0px 5px rgba(0,0,0,.3);
	box-shadow: 0px 0px 5px rgba(0,0,0,.3);
	clear: both;
	margin: 20px 0px;
}

.ck_form, .ck_form * {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

#ck_subscribe_form {
  clear: both;
}

/* Element Queries — uses JS */

.ck_form_content, .ck_form_fields {
	width: 50%;
	float: left;
	padding: 5%;
}

.ck_form.ck_horizontal {
}

.ck_form_content {
	border-bottom: none;
}

.ck_form.ck_vertical {
	background: #fff;
}

.ck_vertical .ck_form_content, .ck_vertical .ck_form_fields {
	padding: 10%;
	width: 100%;
	float: none;
}

.ck_vertical .ck_form_content {
	border-bottom: 1px dotted #aaa;
	overflow: hidden;
}

/* Trigger the vertical layout with media queries as well */

@media all and (max-width: 499px) {

	.ck_form {
		background: #fff;
	}

	.ck_form_content, .ck_form_fields {
		padding: 10%;
		width: 100%;
		float: none;
	}

	.ck_form_content {
		border-bottom: 1px dotted #aaa;
	}

}

/* Content */

.ck_form_content h3 {
	margin: 0px 0px 15px;
	font-size: 24px;
	padding: 0px;
}


.ck_form_content p {
	font-size: 14px;
}

.ck_image {
	float: left;
	margin-right: 5px;
}

/* Form fields */

.ck_errorArea {
	display: none;
}

#ck_success_msg {
	padding: 10px 10px 0px;
	border: solid 1px #ddd;
	background: #eee;
}

.ck_label {
	font-size: 14px;
	font-weight: bold;
}

.ck_form input[type="text"], .ck_form input[type="email"] {
	font-size: 14px;
	padding: 10px 8px;
	width: 100%;
	border: 1px solid #d6d6d6; /* stroke */
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px; /* border radius */
	background-color: #f8f7f7; /* layer fill content */
	margin-bottom: 5px;
	height: auto;
}

.ck_form input[type="text"]:focus, .ck_form input[type="email"]:focus {
	outline: none;
	border-color: #aaa;
}

.ck_checkbox {
  padding: 10px 0px 10px 20px;
  display: block;
  clear: both;
}

.ck_checkbox input.optIn {
  margin-left: -20px;
  margin-top: 0;
}
.ck_form .ck_opt_in_prompt {
  margin-left: 4px;
}
.ck_form .ck_opt_in_prompt p {
  display: inline;
}

.ck_form .ck_subscribe_button {
    width: 100%;
    color: #fff;
    margin: 10px 0px 0px;
    padding:  10px 0px;
    font-size: 18px;
    background: #0d6db8;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px; /* border radius */
    cursor: pointer;
    border: none;
    text-shadow: none;
  }


.ck_form .ck_guarantee {
	color: #626262;
	font-size: 12px;
	text-align: center;
	padding: 5px 0px;
	display: block;
}

.ck_form .ck_powered_by {
	display: block;
	color: #aaa;
}

.ck_form .ck_powered_by:hover {
	display: block;
	color: #444;
}

.ck_converted_content {
  display: none;
	padding: 5%;
	background: #fff;
}


/* v6 */

.ck_form_v6 #ck_success_msg {
	padding: 0px 10px;
}

@media all and (max-width: 403px) {
  .ck_form_v6.ck_modal .ck_close_link {
    top: 30px;
  }
}

@media all and (min-width: 404px) and (max-width: 499px) {
  .ck_form_v6.ck_modal .ck_close_link {
    top: 57px;
  }
}





</style>
</div><script defer="defer" src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/466666" id="_ck_466666"></script></section>
  </div>

  
  
</div>

<div class="l-wrap">
  <div class="post-layout">
    <nav class="post-nav">
      

          <a class="button" data-type="outline" href="https://zellwk.com/blog/windows-wsl/" data-link-to="previous-post">
              <svg height="1em" viewBox="0 0 22 38">
                <path fill="currentColor" d="M18.9 38l3.1-3.1L6.1 19 22 3.1 18.9 0 0 19z"></path>
              </svg>
              <span>Setting up Windows for web development</span>
            </a>
          <a class="button" data-type="outline" href="https://zellwk.com/blog/build-your-developer-brand/" data-link-to="next-post">
              <span>Free Workshop on Branding and Marketing yourself as a developer</span>
              <svg height="1em" viewBox="0 0 22 38">
                <path fill="currentColor" d="M3.1 0L0 3.1 15.9 19 0 34.9 3.1 38 22 19z"></path>
              </svg>
            </a>
          </nav>
  </div>
</div>
</main><footer class="c-footer" role="contentinfo">
  <div class="l-wrap">
    <div class="l-wrap__full l-footer__wrap">
      <div class="c-footer__cta">
        <ul class="c-footer__nav">
          <li>
            <span>About Zell</span>
            <ul>
              <li>
                <a href="https://zellwk.com/">Home</a>
              </li>
              <li>
                <a href="https://zellwk.com/about">About</a>
              </li>
              <li>
                <a href="https://zellwk.com/contact">Contact</a>
              </li>
            </ul>
          </li>

          <li>
            <span>Things I made</span>
            <ul>
              <li>
                <a href="https://zellwk.com/products#course-section">Courses</a>
              </li>
              <li>
                <a href="https://zellwk.com/products#library-section">Libraries</a>
              </li>
            </ul>
          </li>

          <li>
            <span>Newsletter</span>
            <ul>
              <li>
                <a href="https://zellwk.com/newsletter">Email</a>
              </li>
              <li>
                <a href="https://zellwk.com/rss">RSS</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>

      <div class="c-footer__info">
        <p>
          <span>
            © 2020 <a href="https://zellwk.com/blog">Zell Liew</a>
            · <a href="https://zellwk.com/terms">Terms</a>
          </span>
        </p>
      </div>
    </div>
  </div>
</footer>
</div><div class="c-offsite-container jsOffsiteContainer">
  <div class="c-offsite">
    <div class="c-offsite__header">
      <a class="c-main-nav__logo" href="https://zellwk.com/about">
        <div class="o-zell--white"><svg id="zell" viewBox="0 0 90 30">
  <title> Zell </title>
  <g>
    <g class="z">
      <path class="o-zell__bar" d="M0 0v6.332h24.336L24.384 0z"></path>
      <path class="o-zell__text" d="M24.336 6.332h-8.154L0 23.425v6.332h24.336v-6.38H9.562z"></path>
    </g>
    <path class="e o-zell__text" d="M45.706 29.757v-5.34H34.657v-3.614h9.194v-4.941h-9.194V12.28h11.05V6.956H27.893v22.801z"></path>
    <path class="l1 o-zell__text" d="M68.124 29.757v-5.9H57.938V6.956h-7.275v22.801z"></path>
    <path class="l2 o-zell__text" d="M89.646 29.757v-5.9H79.461V6.956h-7.276v22.801z"></path>
  </g>
</svg>
</div>
      </a>
      <a href="#" class="c-offsite__close jsOffsiteClose">×</a>
    </div>
    <div class="c-offsite__content"><nav class="c-offsite__nav">
    <a href="https://zellwk.com/about" class="o-navlink--offsite">About</a>
    <a href="https://zellwk.com/blog" class="o-navlink--offsite">Articles</a>
    <a href="https://zellwk.com/newsletter" class="o-navlink--offsite">Newsletter</a>
    <a href="https://zellwk.com/contact" class="o-navlink--offsite">Contact</a>
  </nav></div>
  </div>
</div>
<div class="c-modal-container jsModalContainer">
  <div class="l-wrap">
    <div class="c-modal jsModal jsLoader">
      <h3 class="c-modal__title jsModalTitle jsLoaderTitle">Hold on while i sign you up…</h3>

      <div class="c-modal__content c-post">
        <div class="c-loader">
          <div class="c-loader__spinner-container">
            <div class="c-loader__spinner o-spinner jsSpinner">
              <div class="o-spinner__item--pink"></div>
              <div class="o-spinner__item"></div>
              <div class="o-spinner__item"></div>
            </div>
            <div class="c-loader__success-icon jsSuccessIcon">🤗</div>
          </div>

          <div class="c-loader__messages jsLoaderMessages jsModalMessages">
            <div class="jsModalSuccessText">Woohoo! You’re in!</div>
            <div>Now, hold on while I redirect you.</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    
    
      <script module="" src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/main-1b7c954c92.mjs"></script>
      <script nomodule="" src="Building%20a%20Simple%20CRUD%20app%20with%20Node,%20Express,%20and%20MongoDB%20|%20Zell%20Liew_files/main-legacy-428160f964.js"></script>

      <script defer="defer">
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
          },
          i[r].l = 1 * new Date();
          a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m
            .parentNode
            .insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-34947087-3', 'auto');
        ga('send', 'pageview');
      </script>
  

</body></html>